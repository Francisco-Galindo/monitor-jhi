import { Component, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {IMqttMessage} from 'ngx-mqtt';
import {Subscription} from 'rxjs';
import {MqttjsService} from 'src/app/services/mqttjs.service';
import {ConnectionDialogComponent} from 'src/app/shared/connection-dialog/connection-dialog.component';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit, OnDestroy, OnChanges {
  connected: boolean = true;
  subscription: Subscription | undefined;
  robots: any[] = [];
  currRobot: any;
  pageName: string = "Monitor de piso"

  constructor(private readonly mqttjsService: MqttjsService,
             private dialog: MatDialog) {
  }

  ngOnDestroy() {
    this.closeConnection();
  }

  ngOnInit() {
    this.mqttjsService.getState().subscribe({
      next: (connected) => {
        this.connected = connected;
        this.closeConnection()
        if (!connected) {
          console.log('sin conexion');
        } else {
          console.log('conectao');
          this.mqttjsService.publish('test/1', 'test');
          this.suscribirse('posiciones/robots');
        }
      }
    })
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  suscribirse(topicName: string) {
    this.robots = [];
    this.subscription = this.mqttjsService.topic(topicName)
      .subscribe((data: IMqttMessage) => {
        let payloadObj = JSON.parse(new TextDecoder().decode(data.payload));
        let obj = this.robots.find(el => el.id === payloadObj.id);

        if (!obj) {
          payloadObj.count = 0;
          this.robots.push(payloadObj);
        } else {
          const oldX = obj.x;
          const oldY = obj.y;

          obj.y = payloadObj.y;
          obj.x = payloadObj.x;

          if (oldX == obj.x && oldY == obj.y) {
            obj.count++;
          } else {
            obj.count = 0;
          }

          obj.failed = (obj.count >= 3);

          obj.batt = payloadObj.batt;
        }

        this.robots = JSON.parse(JSON.stringify(this.robots));
      })
  }

  closeConnection() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = undefined;
    }
  }

  goToRobot(robot: any) {
    if (this.currRobot === robot) {
      this.currRobot = JSON.parse(JSON.stringify(robot));
    } else {
      this.currRobot = robot;
    }
  }

  openConnectionDialog() {
    const dialogRef = this.dialog.open(ConnectionDialogComponent, {});
  }
}
