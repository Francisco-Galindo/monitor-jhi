import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralComponent } from './pages/general/general.component';

import { MonitorRoutingModule } from './monitor-routing.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    GeneralComponent,
  ],
  imports: [
    CommonModule,
    MonitorRoutingModule,
    SharedModule,
  ]
})
export class MonitorModule { }
