import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import Panzoom, {PanzoomEvent, PanzoomObject} from '@panzoom/panzoom';

@Component({
  selector: 'app-map-viewer',
  templateUrl: './map-viewer.component.html',
  styleUrls: ['./map-viewer.component.css']
})
export class MapViewerComponent implements AfterViewInit, OnInit, OnChanges {
  @Input() robots: any[] = [];
  @Input() currRobot: any;

  elem: HTMLElement | null = null;
  container: HTMLElement | null = null;
  panzoom: PanzoomObject | null = null;

  ngOnInit() {
    const circles = Array.from(document.getElementsByTagNameNS(
      "http://www.w3.org/2000/svg",
      "circle"
    ));
    for (const circle of circles) {
      circle.remove();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["currRobot"]) {
      this.focusOnRobot(this.currRobot);
    }

    if (changes["robots"]) {
      if (this.robots.length === 0) {
        this.ngOnInit();
      }

      let circle: HTMLElement | SVGCircleElement | null;
      for (const robot of this.robots) {
        circle = document.getElementById(robot.id);
        if (!circle) {
          circle = document.createElementNS(
            "http://www.w3.org/2000/svg", "circle"
          );


          circle.setAttribute("id", robot.id);
          circle.setAttribute("r", '3');
          circle.setAttribute("stroke", 'black');
          circle.setAttribute("stroke-width", '0.6');
          circle.style.transition = "150ms ease"

          this.elem!.appendChild(circle);
        }
        circle.setAttribute("cx", robot.x);
        circle.setAttribute("cy", robot.y);

        if (robot.failed) {
          const redColor = 'hsl(0, 75%, 50%)';
          robot.color = redColor;
          circle.setAttribute("fill", redColor);

        } else {
          const color = this.strToColor(robot.id);
          if (color !== robot.color) {
            robot.color = color;
            circle.setAttribute("fill", color);
          }
        }
      }
    }
  }

  ngAfterViewInit() {
    this.elem = document.getElementById('panzoom-element');
    this.container = document.getElementById('svg-container');
    // let paths = document.getElementsByTagName('path');
    // Array.from(paths).forEach((path) => {
    //   path.style.filter = "invert(100%)";
    //   path.style.webkitFilter = "invert(100%)";
    // });

    this.panzoom = Panzoom(this.elem!, {
      canvas: true,
      maxScale: 2,
      animate: true,
      duration: 200,
      // origin: '0 0'
      // contain: 'outside',
    })

    this.elem!.parentElement!.addEventListener('wheel', (event) => {
      if (!event.shiftKey) {
        return;
      }

      this.panzoom!.zoomWithWheel(event);
    })

  }

  reset() {
    this.panzoom!.reset();
  }

  slideZoom(n: number) {
    this.panzoom?.zoom(n);
  }

  strToColor(str: string, saturation = 50, lightness = 50) {
    let hash = 0;
    let char;
    for (let i = 0; i < str.length; i++) {
      char = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + char;
      hash |= 0;
    }

    // Evitar que cadenas muy similares tengan hashes muy parecidos
    hash *= 2417;

    // Evitar que aparezcan colores rojos
    hash = (hash % (360 - 40)) + 20;

    const color = `hsl(${hash}, ${saturation}%, ${lightness}%)`;

    return color;
  }

  focusOnRobot(robot: any) {
    if (!robot) {
      return;
    }

    this.panAndZoom(robot.x, robot.y, this.panzoom!.getOptions().maxScale!);
  }

  panAndZoom(x: number, y: number, scale: number) {
    let ms = 0;
    if (this.panzoom!.getScale() !== scale) {
      ms = this.panzoom!.getOptions().duration!;
    }

    this.panzoom!.zoom(scale);
    setTimeout(() => {this.pan(x, y, scale)}, ms);
  }

  async pan(x: number, y: number, scale: number) {
    let svgWidth = parseInt(
      this.elem!.getAttribute('width')!.replace(/\D/g, '')
    );
    let svgHeight = parseInt(
      this.elem!.getAttribute('height')!.replace(/\D/g, '')
    );

    const parentWidth = this.container!.clientWidth;
    const parentHeight = this.container!.clientHeight;

    let {width, height} = this.elem!.getBoundingClientRect();
    width /= scale;
    height /= scale;

    let actualX = (x / svgWidth) * width;
    let actualY = (y / svgHeight) * height;

    const offsetX = (width - parentWidth) / (2 * scale);
    const offsetY = (height - parentHeight) / (2 * scale);

    let panX = (width / 2) - actualX - offsetX;
    let panY = (height / 2) - actualY - offsetY;

    this.panzoom!.pan(panX, panY);
  }
}
