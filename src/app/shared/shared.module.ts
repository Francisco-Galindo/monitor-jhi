import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatCommonModule} from '@angular/material/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatRippleModule} from '@angular/material/core';
import {MatSliderModule} from '@angular/material/slider';
import {MatToolbarModule} from '@angular/material/toolbar';

import { MapViewerComponent } from './map-viewer/map-viewer.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ConnectionDialogComponent } from './connection-dialog/connection-dialog.component';


const materialModules = [
  MatButtonModule,
  MatCardModule,
  MatCommonModule,
  MatDialogModule,
  MatDividerModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatProgressBarModule,
  MatRippleModule,
  MatSliderModule,
  MatSnackBarModule,
  MatToolbarModule,
]


@NgModule({
  declarations: [
    MapViewerComponent,
    ToolbarComponent,
    ConnectionDialogComponent,
  ],
  imports: [
    CommonModule,
    materialModules,
    ReactiveFormsModule,
  ],
  exports: [
    MapViewerComponent,
    ToolbarComponent,
    ConnectionDialogComponent,
    materialModules,
  ]
})
export class SharedModule { }
