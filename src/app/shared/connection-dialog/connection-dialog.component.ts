import { Component, Inject, OnInit } from '@angular/core';
import { FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';

import { MqttjsService } from 'src/app/services/mqttjs.service';

@Component({
  selector: 'app-connection-dialog',
  templateUrl: './connection-dialog.component.html',
  styleUrls: ['./connection-dialog.component.css']
})
export class ConnectionDialogComponent implements OnInit {

  waitingConnection: boolean = false;

  constructor(private dialogRef: MatDialogRef<ConnectionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private _snackbar: MatSnackBar,
              private fb: FormBuilder,
              private mqttjsService: MqttjsService
             ) { }

  ngOnInit(): void {
    this.mqttjsService.getState().subscribe((val) => {
      if (this.waitingConnection) {
        let string = '';
        if (val) {
          string = "Conexión exitosa"
        } else {
          string = "Conexión fallida..."
        }
        this._snackbar.open(string, "Entendido", {
          duration: 3000
        });
        this.waitingConnection = false;
      }
    })
  }

  connectionForm: FormGroup = this.fb.group({
    hostname: [, Validators.required],
    port: [, Validators.required],
    path: [],
  })

  connect() {
    this.waitingConnection = true;
    this.mqttjsService.connect(this.connectionForm.value);
  }
}
