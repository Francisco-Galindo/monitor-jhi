import { Component, Input } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

import { ConnectionDialogComponent } from '../connection-dialog/connection-dialog.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  @Input() pageName: string = "";

  constructor(private dialog: MatDialog) {}

  openConnectionDialog() {
    const dialogRef = this.dialog.open(ConnectionDialogComponent, {});
  }

}
