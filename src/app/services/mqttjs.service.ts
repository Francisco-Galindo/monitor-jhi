import { Injectable } from '@angular/core';
import { IMqttMessage, IMqttServiceOptions, MqttService } from "ngx-mqtt";
import { BehaviorSubject, Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class MqttjsService {

  conectionStatus: boolean = false;
  connectionSubject: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(this.conectionStatus);

  constructor(private _mqttService: MqttService) {
    this._mqttService.onConnect.subscribe(() => {
      this.conectionStatus = true;
      this.connectionSubject.next(this.conectionStatus);
    })

    this._mqttService.onError.subscribe(() => {
      this.conectionStatus = false;
      this.connectionSubject.next(this.conectionStatus);
    })

    this.connect({
      hostname: '192.168.50.174',
      port: 9001,
      path: '',
    });
  }

  getState(){
    return this.connectionSubject;
  }

  connect(opts?: IMqttServiceOptions) {
    this._mqttService.connect(opts);
  }

  topic(topicName: string): Observable<IMqttMessage> {
    return this._mqttService.observe(topicName);
  }

  publish(topicName: string, msg: string) {
    this._mqttService.unsafePublish(topicName, msg, {qos: 0});
    console.log(this._mqttService.state);
  }
}
